This program helps you compare the ABI of two checked out trees, built with
BuildStream.

It roughly does the following:

1.  checkout the reference Git revision,
2.  build the specified element,
3.  checkout the just-built reference tree,
4.  checkout the new Git revision,
5.  build the same element,
6.  checkout the just-built new tree,
7.  compare all the found libraries in the 2 trees with [`abidiff`](https://sourceware.org/libabigail/manual/abidiff.html)

## Usage

Let's say you want to ensure the changes in the `feature/something-cool` branch
do not break the API when compared with the `stable` branch.

In addition, let's say the element to build in order to generate the trees to
compare is named `abi.bst`.

You would run the command as follows:

```
$ ./check-abi --old=feature/something-cool --new=stable abi.bst
```

All details on the available options can be found with the `--help` command-line
argument.

## License

This project is offered under the terms of the
[GNU General Public License, version 3 or any later version][gpl], see
the [COPYING](COPYING) file for details.

[gpl]: https://www.gnu.org/licenses/gpl.html

## History

This project was initially a script in the
[Freedesktop SDK](https://freedesktop-sdk.io/) repository.

Eventually it became clear that other projects build with BuildStream, like
[gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/), wanted to
reuse this to ensure they didn't break their own ABI, and so this was split out
to live independently and be easier to reuse.

## Acknowledgments

This would not exist without the excellent
[libabigail](https://sourceware.org/libabigail/), a library to analyze ABIs.
